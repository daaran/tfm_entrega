var mongoConfig = require('../mongodb.json');

const MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;


var uri = 'mongodb+srv://' + mongoConfig.user + ':' + mongoConfig.pass + '@' + mongoConfig.host + '/' + mongoConfig.db ;

(function() {

  async function getDoc( pDocId, pCollectionName ) {

    let client = await MongoClient.connect(uri );
    try {
      let collection = client.db("reports").collection( pCollectionName );
      let docContent = ( await collection.findOne(
        {
          "_id": new ObjectId(pDocId)
        })
      );
      return docContent;  
    }
    catch(e){
      console.log( e )
    }
    finally {
      client.close();
    }
  }


  module.exports.getDoc = getDoc;


})();


// if( mongoConfig.remote ){}

// let docs = collection.find({}, {
//   limit: 1000
// }).toArray(function (err, docs) {
//   console.log( docs )
//   return docs;
// });
