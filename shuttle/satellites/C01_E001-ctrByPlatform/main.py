 # --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultiLayerTimeSerie import MultiLayerTimeSerie
multilayer = MultiLayerTimeSerie()
reporter.builder = multilayer.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()

    postalCodes =  inputs[1]["path"][0]
    dfPostalCodes = exporter.csvToDF( postalCodes )

    #Input Platfomrs files:
    inputPlatforms = ["Facebook","Instagaram","Twitter","Youtube"]

    platformId = params["platformId"]; 
    platformName = inputPlatforms[ platformId ]

    socialSource =  inputs[0]["path"][ platformId ]
    dfFB = exporter.csvToDF( socialSource )

    #Process
    dfFB.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    dfFB["City"] = social.addColumn_CitynameByZipcode( dfFB, dfPostalCodes )

    pListFilter = ['City',  'Date'  ]  # 'Product',

    pDf = dfFB
    mP = pDf.groupby( pListFilter )['Prints'].mean().round( decimals= 2)
    mH = pDf.groupby( pListFilter )['Hits'].mean().round( decimals= 2)
    rDf = pd.concat([ mP , mH], axis=1, keys=['meanPrints', 'meanHits'])
    rDf.reset_index( inplace=True)
    
    # #rDf['ctr'] = np.vectorize( social.divisionCtr )( rDf['meanPrints'], rDf['meanHits'] )
    
    rDf['ctr'] = rDf.apply(lambda row: social.divisionCtr( row['meanPrints'],row['meanHits'])  , axis=1 )
    # print rDf.head(1)
    # '''
    #     City        Date        meanPrints  meanHits   ctr
    #  0  Barcelona  2017-01-01   95.27       13.79      6.91
    # '''

    rDf[['year', 'month', 'day']]= rDf["Date"].str.split("-",  expand = True)

    outputParams =  reloader.getOutputPath( platformName )
    exporter.dfToCsv( rDf, outputParams["path"] )
    metaResult = reloader.saveMetas() 


        
        


    

     