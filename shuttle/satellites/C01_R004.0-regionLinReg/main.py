 # --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultipleGroupsXY import MultipleGroupsXY
multipleGroupsXY = MultipleGroupsXY()
reporter.builder = multipleGroupsXY.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv
import re

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()
    regionId = params["regionId"]

    citySource =  inputs[0]["path"][ regionId ]
    dfCity = exporter.csvToDF( citySource )

    cityMatch = re.search('.*_.+[0-9]{3}_(.*).csv', citySource )
    if not cityMatch is None:
        cityName = cityMatch.group(1)
        print cityName
    else:
        raise ValueError('Not City name match')
 
    groups = [
        { 
            "name":"facebook",
            "type":"platform",
            "x":"prints_facebook",
            "y":"hits_facebook",
            "label_x":"Prints",
            "label_y":"Hits"
        },
        { 
            "name":"instagram",
            "type":"platform",
            "x":"prints_instagram",
            "y":"hits_instagram",
            "label_x":"Prints",
            "label_y":"Hits"
        },
        { 
            "name":"twitter",
            "type":"platform",
            "x":"prints_twitter",
            "y":"hits_twitter",
            "label_x":"Prints",
            "label_y":"Hits"
        },
        { 
            "name":"youtube",
            "type":"platform",
            "x":"prints_youtube",
            "y":"hits_youtube",
            "label_x":"Prints",
            "label_y":"Hits"
        }
    ]

    outputParams =  reloader.getOutputPath(  cityName + "-linreg" )
    
    blockKey = "City"
    builderObj = reporter.builder( dfCity, groups, blockKey )

    
    if not reloader.isMongoTarget() :
        exporter.objToJson( builderObj, outputParams["path"] )
        metaResult = reloader.saveMetas() 
    else:
        #Save MongoDB
        insertId =  reporter.jsonToReport( outputParams["collection"], builderObj )
        print insertId
        
        reloader.addMetaOutputDoc( insertId )

        metaResult = reloader.saveMetas()
        #metaData = reloader.getMetadata()
        
        reporter.addMetadataToDoc( insertId, outputParams["collection"],  metaResult )

        
        


    

     