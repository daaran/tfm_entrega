#!/usr/local/bin/python
import sys
import os
import json
import urllib2, base64
import simplejson
import datetime as dtm
import pandas as pd
from pandas.compat import StringIO
from os import listdir
from os.path import isfile, join

'''
loadMeta()
setRoutes( u, r, s)

getScript()
getParams( )
getInput( prop )
getOutput( prop )
getComment(  )
getInputFull( )
getOutputFullWithSuffix( )
today( str_format, dif )
setMetaAttr( prop, val )
createResultMetada( )
parseArgs( argsList )
getMasterRegistry( )

'''


'''
USAGE:
inputFiles = loader.getInput( "file" )

print loader.getScript( "description" )
loader.setRoutes( u= universe );
listInputFiles =  loader.getInputFull()[0]
print listInputFiles
print loader.getOutputFullWithSuffix( 'PRODUCTS' )
loader.setMetaAttr( "inputFile", listInputFiles  )
loader.createResultMetada();

!! add master in loader instance
registry = loader.getMasterRegistry()
dfPostalCodes = exporter.csvToDF( registry["test-postalcode"] + "001_2018.05.03_ZONES.csv" )

'''

class Loader(object):

    def __init__(self, ROOT_DIR, CURRENT_DIR, ROUTES ):
        self.dir_path = os.path.dirname( os.path.realpath( __file__ ) ) 
        self.rootDir =  ROOT_DIR
        self.currentDir = CURRENT_DIR
        self.meta = {}

        self.universe = ROUTES["universe"]
        self.processed = ROUTES["processed"]
        self.stage = ROUTES["stage"]
        self.master = ROUTES["master"]
        self.satellites = ROUTES["satellites"]
        self.reports = ROUTES["reports"]

        self.inputPathFull = ""
        self.putputPathFull = ""
        self.outputCounter = 0 
        self.fullOutputPath = ""
        self.metaOut = {
            "comment": "", 
            "dateCreation": "", 
            "description": "", 
            "inputFile": "", 
            "inputPath": "", 
            "inputDir": "", 
            "outputPath": "", 
            "outputDir": "", 
            "routineName": "", 
            "version": "",
            "params":{}
        }
        self.requiredToMetadata = False
        self.params = {}
        self.loadMeta( )

    def getMasterRegistry( self ):
        if self.master == '':
            raise ValueError('MASTER not definned')
        masterRegistry= 'registry.json'
 
        jsonRegistryFile = open(os.path.join( self.master, masterRegistry ), 'r')
        jsonRegistry = json.load( jsonRegistryFile )
        
        globalRegistry = {}
        for regKey, regAttrs in jsonRegistry["datasets"].iteritems():
            route =  self.master + regAttrs["dir"] + "data/"
            globalRegistry[ regKey ] = route.replace("/", "\\")
        return globalRegistry


    def loadMeta( self ):
        modelFileReference= 'metascript.json'
        rawJsonModel = open(os.path.join( self.currentDir, modelFileReference ), 'r')
        self.meta = json.load( rawJsonModel )
        self.setMetaAttr("inputFile", self.meta["input"][ "file" ] )

    def setRoutes( self, u="", r="", s=""):
        self.universe = u
        self.reports = r
        self.satellites = s

    def getScript( self, prop ):
        return self.meta["script"][ prop ]
    
    def getParams(self ):
        return self.meta["params"]
    
    def getInput( self, prop ):
        
        return self.meta["input"][ prop ]
    
    def getOutput( self,prop ):
        return self.meta["output"][ prop ]
    
    def getComment( self ):
        return self.meta["comment"]


    def getInputFull( self, pListFiles ):
        fullInputPath = ""

        if( self.getInput( "dir" ) == "processed" ):
            fullInputPath += self.processed
            self.setMetaAttr( "inputDir", "processed" )

        elif( self.getInput( "dir" ) == "stage" ) :
            fullInputPath += self.stage
            self.setMetaAttr( "inputDir", "stage" )

        elif( self.getInput( "dir" ) == "master" ) :
            fullInputPath += self.master
            self.setMetaAttr( "inputDir", "master" )
        
        elif( self.getInput( "dir" ) == "reports" ) :
            fullInputPath += self.reports
            self.setMetaAttr( "inputDir", "reports" )
        else:
            return False

        fullInputPath += self.getInput( "path" ).replace("/", "\\")
        self.setMetaAttr( "inputPath", self.getInput( "path" ) )
        def prependPath(fileName):
                return fullInputPath + fileName
        #e = self.getInput( "file" )
        if isinstance(pListFiles, list):
            return map(prependPath, pListFiles )
        else:
            return prependPath( pListFiles )
            
    

    def getOutputFullWithSuffix( self, addon="" ):
        fullOutputPath = ""

        if( self.getOutput( "dir" ) == "processed" ):
            fullOutputPath += self.processed
            self.setMetaAttr( "outputDir", "processed" )
        
        elif( self.getOutput( "dir" ) == "stage" ):
            fullOutputPath += self.stage
            self.setMetaAttr( "outputDir", "stage" )

        elif( self.getOutput( "dir" ) == "master" ):
            fullOutputPath += self.master
            self.setMetaAttr( "outputDir", "master" )

        elif( self.getOutput( "dir" ) == "reports" ):
            fullOutputPath += self.reports
            self.setMetaAttr( "outputDir", "reports" )
        else:
            return False
        
        fullOutputPath += self.getScript( "name" ) +  '\\'

        self.createOutputDir( fullOutputPath )
        self.fullOutputPath = fullOutputPath
        self.requiredToMetadata = True

        self.setMetaAttr( "outputPath", self.getScript( "name" ) +  '\\' )
        

        #@todo: create directory if not exist
        prefix = self.getScript( "name" ).split("-")[0]
        self.setMetaAttr("routineName", self.getScript( "name" ) )
        
        fullOutputPath += prefix

        if( self.getOutput( "addDate" ) ):
            fullOutputPath += '_' + self.today()
        
        if( addon != "" ):
            fullOutputPath += '_' +  addon  

        return fullOutputPath


    def today(self, str_format = '%Y.%m.%d' , dif=0):
        #http://strftime.org/
        today = dtm.datetime.today() - dtm.timedelta( ( int(dif)*-1) )
        DATE_TODAY = dtm.datetime.strftime(today, str_format)
        return DATE_TODAY

    def createOutputDir(self, DIR  ):
        if not os.path.exists( DIR ):
            print 'folder created'
            os.makedirs( DIR )
        else:
            print "folder exist"

    def setMetaAttr( self, prop="", val="" ):
        if( prop in self.metaOut ):
            self.metaOut[ prop ] = val;
        else:
            print  prop + " prop not exist in model" 

    def createResultMetada( self ):
        if( self.requiredToMetadata ):
            
            self.metaOut["inputPath"] = self.metaOut["inputPath"].replace( self.rootDir ,"").replace( "\\","/")
            self.metaOut["outputPath"] = self.metaOut["outputPath"].replace( self.rootDir ,"").replace( "\\","/")

            self.setMetaAttr( "dateCreation", self.today()  )
            model = self.metaOut
            with open( self.fullOutputPath + 'metadata.json', 'w') as outfile:
                json.dump( model, outfile , indent=2, sort_keys=True)
        else:
            self.getOutputFullWithSuffix()
            self.createResultMetada()
            #print "No required metadas props, move % createResultMetada % after % getOutputFullWithSuffix %"
    

    def parseArgs( self, args ):
        
        metadataParams =  self.getParams()
        numRequiredParams = 0;
        numIntroducedRequiredParams = 0
   
        for paramKey, paramAttrs in metadataParams.iteritems():
            if paramAttrs["required"]:
                numRequiredParams +=1
  
        for arg in args:
            param = arg.split("=")
            argKey = param[0]
            argValue = param[1]
            self.params[ argKey ]= argValue
            # Check in arg is required
            if argKey in metadataParams:
                if metadataParams[ argKey ]["required"]:
                    numIntroducedRequiredParams += 1
                # To output metada add params with gived value
                metadataParams[ argKey ]["value"] = argValue
            else:
                print ">> ignored argument: ", argKey
    

        if numIntroducedRequiredParams < numRequiredParams:
            raise ValueError('Not all required parameters')



        self.setMetaAttr("params", metadataParams )

        return self.params
