#!/usr/local/bin/python
import sys
import urllib, json
import numpy as np
import pandas as pd
import math
import itertools
import string
import copy
import collections
import time
import datetime as dt
from ast import literal_eval
import csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os
os.chdir( os.path.dirname(__file__) )
from os.path import isfile, join



class Social( object ):

    def __init__( self ):
        #init
        self.dir_path = os.path.dirname( os.path.realpath( __file__ ) )
        #print 'walk, don`t run'

    def divisionCtr( self, p, h ):
        #print p/h
        if h != 0:
            resDiv =  round( (( float( h) /  float(p) ) *100)  , 2 )
        else:
            resDiv = 0
        return resDiv
        
    def divisionCtrFalse( self, p, h ):
        #print p/h
        if h != 0:
            resDiv =  round( float(p) / float( h)  , 2 )
        else:
            resDiv = 0
        return resDiv

    # def calculateCtr( self, pDf ):
    #     mP = pDf.groupby(['Zip Code'])['Prints'].mean().round( decimals= 2)
    #     mH = pDf.groupby(['Zip Code'])['Hits'].mean().round( decimals= 2)
    #     rDf = pd.concat([ mP , mH], axis=1, keys=['meanPrints', 'meanHits'])
    #     rDf.reset_index(level=0, inplace=True)
    #     rDf.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    #     rDf['ctr'] = np.vectorize( self.divisionCtr )(rDf['meanPrints'], rDf['meanHits'])
    #     return rDf
    

    
    def addColumn_CitynameByZipcode( self, dfTarget, dfZones ):
        # print dfZones.dtypes
        return dfTarget['zipCode'].map( dfZones.set_index('ZipCode')['City'] )

    
    def addColumnByJoin( self, dfTarget, dfAgg, pJoinColumn, pAggColum ):
        return dfTarget[pJoinColumn].map( dfAgg.set_index(pJoinColumn)[ pAggColum ] )










'''
itemDay = dfC[ dfC["date_int"] == int(itemDate) ]["day_name"].values[0]
itemDayWeek = dfC[ dfC["date_int"] == int(itemDate) ]["dayWeek"].values[0]
formatedDate = itemDate[0:4] + "." + itemDate[4:6] + "." + itemDate[6:8] 


dfCounter = pd.DataFrame(list(countedElements.iteritems()),   columns=['number','counter'])
dfCounter[['number']] = dfCounter[['number']].apply(pd.to_numeric)

dfD = dfN.filter(['date','dayWeek'], axis=1)

def cleanDate( x ):
    dateFormated = x.replace('-','')
    return dateFormated 
dfD['date_int'] = dfD['date'].apply( lambda x: cleanDate(x) )

'''

'''
def selectColorRange( x ):
    
    if x<10:
        color = 'r'
    elif x >= 10 and x <=15:
        color = 'g'
    else:
        color = 'b'
    
    print str(x) + " " + color 

    return color

dfCounter["color"] = map( selectColorRange ,dfCounter["counter"] )

'''


'''
df2 =  pd.DataFrame( train['gewinnzahlen'].values.tolist() ,  columns=['n1','n2','n3','n4','n5','n6'] )
df2["s"] = train['superzahl'].values.tolist()

df2["id"] = train['date'].values.tolist()
df2['id'] =  df2["id"].map(lambda x: int( x.replace("-","") ) )
'''

'''
df2.sort(['id'], ascending=True)

d2 =  dfN.sort_values( "rate", ascending=False  )

x3with2Df =  pd.DataFrame(x3with2)
x3with2Df.columns = ["combo2","combo3","extra","extra_count_single"]

L15 = dfNCS[ ( dfNCS.counter == 15 ) ].number.values
L12_14 = dfNCS[ ( dfNCS.counter >= 12 ) & ( dfNCS.counter <=14 )  ].number.values

foundNumbersA =  [x for x in nr if x in dfSeq[id].values]

print dfA[ (dfA.counter >= 6 ) & ( dfA.counter <= 10 ) ]["number"].values


#print df2.sort(['count'], ascending=False)
#print  df2.ix[1:9,0:2] 
#print  df2.loc[1:9,["a"]] 
#print  df2.loc[ ((df2["a"] == 23) & (df2["b"] == 34)) | ((df2["b"] == 23) & (df2["a"] == 34))  ] 
#print df2.loc[ df2['a'].isin( [23,27] ) | df2['b'].isin( [23,27] ) ] 
# print  df3["comboItems"]

topSingles =  df1.sort(['counter'], ascending=False).head(15)
topSinglesList =  list( topSingles["number"] )



    print dfYT.groupby(['Zip Code'])['Prints'].mean()
'''