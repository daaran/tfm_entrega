
import numpy as np

class itemRate( object ):
  def __init__(self, intRate ,strName ):
    self.rate = intRate
    self.name = strName


class itemGroup( object ):
  def __init__(self):
    self.data = []
    self.attr = {}

#float(rowData.iloc[0][ dataCols[0] ]),

class SingleSeriesGroupsRate( object ):

    def __init__( self ):
      self.name = "SingleSeriesGroupsRate"
    
    def builder( self, pSourceDf, blockKey, pDivider, pSerieCol,  pRateOfFields, pAggFields ):
     
      buildItem = {}
      totals = {}
      
      #Sum Prints and hits for platforms
      bufSumDf = pSourceDf.groupby([pDivider]).sum()

      uniqueDividers = pSourceDf[ pDivider ].unique()

      for divId , divItem in enumerate( uniqueDividers ):

        totals[ divItem ] = {}
        buildItem[ divItem ] = []

        for rfId, rateFieldKey in enumerate( pRateOfFields ):
          totals[ divItem ][ rateFieldKey ] = int( bufSumDf.loc[ divItem,  rateFieldKey ] )
           

      for index, row in pSourceDf.iterrows():
        
        rowItem = itemGroup()
        rowItem.attr["name"] = row[ pDivider ]
        rowItem.attr[ pSerieCol ] = row[ pSerieCol ]

        bufSerie = itemGroup()

        for rfId, rateFieldKey in enumerate( pRateOfFields ):
          calculatedRate = round( ( row[ rateFieldKey ] * 100) / float( totals[ divItem ][ rateFieldKey ] ), 2 )
          bufSerie.data.append( vars( itemRate( str(calculatedRate), rateFieldKey ) ) ) 
          bufSerie.attr[ "total" ] = str( totals[ divItem ][ rateFieldKey ] )
          bufSerie.attr[ "current" ] = str(  row[ rateFieldKey ] )
        
        for aggId, aggKey in enumerate( pAggFields ):
          bufSerie.attr[ aggKey ] = str(row[ aggKey ])

        rowItem.data.append( vars( bufSerie ) )  

        buildItem[  row[ pDivider ] ].append( vars( rowItem ) )

      return buildItem 

     


