
import numpy as np

class itemRate( object ):
  def __init__(self, intRate ,strName ):
    self.rate = intRate
    self.name = strName


class itemGroup( object ):
  def __init__(self):
    self.data = []
    self.attr = {}

#float(rowData.iloc[0][ dataCols[0] ]),

class SingleGroupsRate( object ):

    def __init__( self ):
      self.name = "SingleGroupsRate"

    def builder( self, pSourceDf, blockKey, pDivider, pRateOfFields, pAggFields ):

        buildItem = {}

        totals = {}
        for rfId, rateFieldKey in enumerate( pRateOfFields ):
            totals[ rateFieldKey ] = pSourceDf[ rateFieldKey ].sum()

        for index, row in pSourceDf.iterrows():

            rowItem = itemGroup()
            rowItem.attr["name"] = row[ pDivider ]
            rowItem.attr["block"] = blockKey
            for rfId, rateFieldKey in enumerate( pRateOfFields ):
              calculatedRate = round( ( row[ rateFieldKey ] * 100) / float( totals[ rateFieldKey ] ), 2 )
              rowItem.data.append( vars( itemRate( str(calculatedRate), rateFieldKey ) ) )

            for aggId, aggKey in enumerate( pAggFields ):
              rowItem.attr[ aggKey ] = str(row[ aggKey ])

            buildItem[  row[ pDivider ] ] = vars( rowItem )

        return buildItem
