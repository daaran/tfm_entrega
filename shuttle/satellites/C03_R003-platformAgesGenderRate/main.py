 # --------------------------------------------------- COMMON LIBS
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd())
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultipleGroupsRate import MultipleGroupsRate
multipleGroupsRate = MultipleGroupsRate()
reporter.builder = multipleGroupsRate.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv
import re

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':

    #Load
    inputs = reloader.getInputs()

    modObj = {}

    for inputId, inputItem in enumerate( inputs[0]["path"]):

        socialSource =  inputs[0]["path"][ inputId ]

        platformMatch = re.search('.*_.+[0-9]{3}_(.*).csv', socialSource )
        if not platformMatch is None:
            platformName = platformMatch.group(1)
        else:
            raise ValueError('Not Platform name match')

        dfSocial = exporter.csvToDF( socialSource )
        dfSocial["Platform"] = platformName

        #El orden de Gender y Age en la lista forma la estructura del arbol en el json resultado
        modObj[ platformName ] = reporter.builder( dfSocial, platformName, ['Gender','Age'], ['Prints', 'Hits'], ['ctr'] )

    #Create Report Object

    outputParams =  reloader.getOutputPath( "platforms_ages-rates" )

    if not reloader.isMongoTarget() :
        exporter.objToJson( modObj, outputParams["path"] )
        metaResult = reloader.saveMetas()
    else:
        #Save MongoDB
        insertId =  reporter.jsonToReport( outputParams["collection"], modObj )
        print insertId

        reloader.addMetaOutputDoc( insertId )

        metaResult = reloader.saveMetas()
        #metaData = reloader.getMetadata()

        reporter.addMetadataToDoc( insertId, outputParams["collection"],  metaResult )
