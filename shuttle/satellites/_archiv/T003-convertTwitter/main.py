# --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)
#scriptName = os.path.basename(__file__)[:-3]

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.loader  import Loader
from lib.helper_01  import Helper
from lib.router  import Router
#......................

router = Router( )

loader = Loader( currentFullRoute, router.getRoutes()   )
helper = Helper( )

sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

# ---------------------------------------------------------------------- MAIN



def printfull(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')


if __name__ == '__main__':
    
    #params = loader.parseArgs( sys.argv[1:] )
    
    inputFiles = loader.getInput( "file" )
    fullInputFiles =  loader.getInputFull( inputFiles )

    for cursor, iFile in enumerate(fullInputFiles):
        
        data = []
        #print iFile
        with open( iFile ) as f:
            for line in f:
                data.append(json.loads(line))
                #print line

        outputFilePathSuffix = loader.getOutputFullWithSuffix( "#" + inputFiles[cursor] )
        modName = outputFilePathSuffix.replace(".txt", ".json")

        with open( modName , 'w') as outfile:
            json.dump( data , outfile , indent=4)

    loader.createResultMetada(); 


   
    # inputFiles = loader.getInput( "file" )
    # fullInputFiles =  loader.getInputFull( inputFiles )

    # outputFilePathSuffix = loader.getOutputFullWithSuffix( "normal" )
    # loader.createResultMetada(); 
    
    # frames = []

    # data1  = json.loads( open( fullInputFiles[0] , "r" ).read() )
    # result1 = json_normalize(data1['tweets'])
    # df1 = pd.DataFrame(result1)
    
    # print df1



    # frames.append( df1 )
    
    # masterDf = pd.concat(frames)

    # newDf = masterDf.loc[:,['id_str','user.id_str', 'created_at','user.description','user.location','user.created_at','retweeted_status.id_str','quoted_status.in_reply_to_user_id_str' ]].copy();

    # newDf.columns= ['a','b','c','d','e','f','g','h']
    # newDf['d'] = newDf.d.str.replace(r'\n', '')

    # outputFile  = outputFilePathSuffix +  '.csv' 
    # newDf.to_csv( outputFile , header=True, sep='|', encoding='utf-8')

     