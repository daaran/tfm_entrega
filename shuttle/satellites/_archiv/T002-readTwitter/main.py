# --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)
#scriptName = os.path.basename(__file__)[:-3]

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )

 # Optional module 2
from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import json
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from connect import *
import regex

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )

outputParams = reloader.getOutputPath( "twitter-" + params["counter"] )


#Use this listener when you just filter by the track stream.filter
class StdOutListener(StreamListener):
    def on_data(self, data):
        with open( outputParams["path"] +  ".txt", "a") as myfile:
            tweet = json.loads(data)
            json_doc = json.dumps(tweet, ensure_ascii=False)
            myfile.write(json_doc.encode('utf8') + '\n') # comma separate items
            #print data
            return True

    def on_error(self, status):
        print status

#Listener with string filter in tweetObj.text to combine locations stream.filter
class GeocodeStreamListener(StreamListener):
    def on_data(self, data):
        tweet = json.loads(data)
        #tweetText = result = regex.sub(ur'[^\p{Latin}]', u'', tweet["text"].lower())  

        tweetText = tweet["text"].lower() #.encode("utf-8")  

        if params["search"].lower() in tweetText:
            print ">>"
            with open( outputParams["path"] +  ".txt", "a") as myfile:
                json_doc = json.dumps(tweet, ensure_ascii=False)
                myfile.write(json_doc.encode('utf8') + '\n') # comma separate items
                #print data
                return True

    def on_error(self, status):
        print status


def isLatin(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True



if __name__ == '__main__':
 
    print params
    print outputParams

    auth = OAuthHandler( consumer_key, consumer_secret )
    auth.set_access_token( access_token, access_token_secret )

    print params["geocode"]
    coorListStr =  params["geocode"].split(",")
    coorListFloat = [float(i) for i in coorListStr]

    if params["geocode"]:
        stream = Stream(auth, GeocodeStreamListener()) 
        locs = coorListFloat
        stream.filter( locations=locs )

    else:
        stream = Stream( auth, listener = StdOutListener( ) )
        stream.filter( track=[ params["search"] ] )


     