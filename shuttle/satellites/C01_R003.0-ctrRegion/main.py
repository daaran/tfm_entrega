 # --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultiLayerTimeSerie import MultiLayerTimeSerie
multilayer = MultiLayerTimeSerie()
reporter.builder = multilayer.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv
import re

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()
    regionId = params["regionId"]
   
    citySource =  inputs[0]["path"][ regionId ]
    dfCity = exporter.csvToDF( citySource )

    cityMatch = re.search('.*_.+[0-9]{3}_(.*).csv', citySource )
    if not cityMatch is None:
        cityName = cityMatch.group(1)
        print cityName
    else:
        raise ValueError('Not City name match')

    outputParams =  reloader.getOutputPath( cityName )
    modObj = reporter.builder( dfCity , 'Date', 'Platform', ['Prints','Hits','ctr'], cityName )
       
    
    if not reloader.isMongoTarget() :
        exporter.objToJson( modObj, outputParams["path"] )
        metaResult = reloader.saveMetas() 
    else:
        #Save MongoDB
        insertId =  reporter.jsonToReport( outputParams["collection"], modObj )
        print insertId
        
        reloader.addMetaOutputDoc( insertId )

        metaResult = reloader.saveMetas() 
        #metaData = reloader.getMetadata()
        
        reporter.addMetadataToDoc( insertId, outputParams["collection"],  metaResult )

        
        


    

     