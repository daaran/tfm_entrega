 # --------------------------------------------------- COMMON LIBS 
import sys
reload(sys) 
sys.setdefaultencoding('utf8')
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultiLayerTimeSerie import MultiLayerTimeSerie
multilayer = MultiLayerTimeSerie()
reporter.builder = multilayer.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()

    postalCodes =  inputs[1]["path"][0]
    dfPostalCodes = exporter.csvToDF( postalCodes )

    uniqueRegions = dfPostalCodes[ "City" ].unique()
    cityName = uniqueRegions[ params["regionId"] ]

    socialSource0 =  inputs[0]["path"][ 0 ]
    socialSource1 =  inputs[0]["path"][ 1 ]
    socialSource2 =  inputs[0]["path"][ 2 ]
    socialSource3 =  inputs[0]["path"][ 3 ]
    
    dfFB = exporter.csvToDF( socialSource0 )
    dfINS = exporter.csvToDF( socialSource1 )
    dfTW = exporter.csvToDF( socialSource2 )
    dfYT = exporter.csvToDF( socialSource3 )


    #Process
    dfFB.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    dfFB["City"] = social.addColumn_CitynameByZipcode( dfFB, dfPostalCodes )
    dfFB["Platform"] = "Facebook"
    dfFBred = dfFB[ dfFB["City"] == cityName ]

    dfINS.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    dfINS["City"] = social.addColumn_CitynameByZipcode( dfINS, dfPostalCodes )
    dfINS["Platform"] = "Instagram"
    dfINSred = dfINS[ dfINS["City"] == cityName ]

    dfTW.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    dfTW["City"] = social.addColumn_CitynameByZipcode( dfTW, dfPostalCodes )
    dfTW["Platform"] = "Twitter"
    dfTWred = dfTW[ dfTW["City"] == cityName ]

    dfYT.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
    dfYT["City"] = social.addColumn_CitynameByZipcode( dfYT, dfPostalCodes )
    dfYT["Platform"] = "Youtube"
    dfYTred = dfYT[ dfYT["City"] == cityName ]

    #print dfFB.describe().to_string()
    # print len( dfFBred )
    # print dfFBred.head(1)

    cityAllPlatformsDF = [dfFBred, dfINSred, dfTWred, dfYTred]
    dfCityAll =  pd.concat(cityAllPlatformsDF)

    #At this point we have all the Platforms-data from a Ciyty in one year

    pListFilter = ['Platform',  'Date'  ]  # 'Product',

    pDf = dfCityAll
    mP = pDf.groupby( pListFilter )['Prints'].sum().round( decimals= 2)
    mH = pDf.groupby( pListFilter )['Hits'].sum().round( decimals= 2)
    rDf = pd.concat([ mP , mH], axis=1, keys=['Prints', 'Hits'])
    rDf.reset_index( inplace=True)
    
    # #rDf['ctr'] = np.vectorize( social.divisionCtr )( rDf['meanPrints'], rDf['meanHits'] )
    rDf['ctr'] = rDf.apply(lambda row: social.divisionCtr( row['Prints'],row['Hits'])  , axis=1 )
    
    
    #Create Report Object
    '''
        Platform      Date            prints    hits    ctr
    0   Facebook      2017-01-01      74.43         9.18        8.11
    1   Facebook      2017-01-02      70.11         10.66       6.58
    '''

    #print cityName
    outputParams =  reloader.getOutputPath( cityName )
    exporter.dfToCsv( rDf, outputParams["path"] )
    metaResult = reloader.saveMetas() 








     