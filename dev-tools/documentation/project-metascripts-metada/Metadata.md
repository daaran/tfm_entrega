# Metadata
>**Version**: 1.0.0

> **Description:**
Define the information about the **dataset** that is in the same directory.

<br>
### Structure 
It contains almost 5 blocks: 
- date_creation
- inputs
- output
- params
- script_name



###### example
```json
{
  "date_creation": "2018.05.01", 
  "inputs": [
    {
      "collection": "DataPubliXSIFGY", 
      "data_source": "dir", 
      "docs": [
        "DataPubliXSIFGYvs2.xlsx"
      ], 
      "domain": "stage"
    }
  ], 
  "output": {
    "collection": "001-readXcel", 
    "data_source": "dir", 
    "docs": [], 
    "domain": "processed"
  }, 
  "params": {}, 
  "script_name": "001-readXcel"
}
```

---
#### date_creation
Creation-date of the output dataset

```json
"date_creation": "2018.05.01", 
```

---

#### inputs

###### Options

| Property | Value | Description |
| ------ | ------ | ------ |
| data_source | - dir <br> - mongo | Reference of the source input data |
|domain| - stage <br> - master <br> - processed <br> - reports <br>  | Reference of the source input data <br> See the Router.py module. |
| collection | string | Name of the MongoDb collection or <br> the folder in repository "domain" |
| docs | array | A list of the source-files |


###### Structure example
```json
{
  "inputs": [
    {
      "collection": "DataPubliXSIFGY", 
      "data_source": "dir", 
      "docs": [
        "DataPubliXSIFGYvs2.xlsx"
      ], 
      "domain": "stage"
    }
  ]
}
```

---

#### output

###### Options

| Property | Value | Description |
| ------ | ------ | ------ |
| data_source | - dir <br> - mongo | Reference of the source input data |
|domain| - stage <br> - master <br> - processed <br> - reports <br>  | Reference of the source input data <br> See the Router.py module. |
| collection | string | Name of the MongoDb collection or <br> the folder in repository "domain" |
| docs | array | A list of the source-files |


###### Structure example
```json
{
  "output": {
    "collection": "001-readXcel", 
    "data_source": "dir", 
    "docs": [], 
    "domain": "processed"
  }, 
}
```
---
#### params
The gived params in the dataset creation

```json
"params": {
    
}, 
```

---
#### script_name
The script name that creates the dataset

```json
"script_name": "001-readXcel"
```

---