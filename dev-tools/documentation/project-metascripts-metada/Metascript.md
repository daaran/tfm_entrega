# Metascript
>**Version**: 1.0.0

> **Description:**
Define the configuration and extra information about the **script** that is in the same directory.

<br>
### Structure 
It contains 5 main blocks: 
- info
- params
- inputs
- output
- cmd

---

#### info
Contiene informacion descriptiva acerca del script, como nomnbre, version, descripcion, fecha de creación y permiso de ejecucion.

###### Usage
```json
"info":{
    "name":"H00-scriptName",
    "version":"1.0",
    "description":"Your description here",
    "comment":"",
    "created":"YYYY.MM.DD",
    "blocked":0
},
```
---

#### params
Define los parametros que pueden ser utilizados en el script, y definidos en el comando de ejecución del script.

```json
"params":{
    "search":{
            "required":true,
            "type":"string",
            "example":"search=#NBA",
            "description":"Your descrition here"
        },
    "counter":{
        "required":false,
        "type":"string",
        "example":"counter=mysuffix",
        "description":"Your descrition here"
    }
},

```

###### Usage

>Required argument:
Throw exception if not added in command, and the script calls the arguments-parser method
```
001-myScriptName/python main.py search=#NBA
```

<br>

> Required and optional arguments:
```
001-myScriptName/python main.py search=#NBA counter=jordan-01
```

<br>

> Non-specified argument: Will be ignored
```
001-myScriptName/python main.py game=hockey
```

---

#### inputs

###### Options

| Property | Value | Description |
| ------ | ------ | ------ |
| data_source | - dir <br> - mongo | Set the source of the input, from a file in folder <br> or a MongoDb doc |
|domain| - stage <br> - master <br> - processed <br> - reports <br>  | Set the sub-area of the source. <br> Mongo and the folder system has the same structure. <br> That means you can combine each domain with the property data-source. <br> See the Router.py module. |
| collection | string | name of the MongoDb collection or <br> the folder in repository "domain" |
| docs | array | a list of docs avaible in the collection or folder |


###### Structure example
```json
 "inputs":[
        {
            "data_source":"dir",
            "domain":"master",
            "collection":"test-postalcode",
            "docs":["001_2018.05.03_ZONES.csv"]
        },
        {
            "data_source":"dir",
            "domain":"processed",
            "collection":"001-readXcel",
            "docs":[ 
                "001_FACEBOOK.csv",
                "001_YOUTUBE.csv"
            ]
        }
    ],
```

###### Access example
See the Reloader.py module, to access the metascript elements.
```python
#Abstract document-access , see example 
faceBookFile = inputs[ 1 ]["docs"][ 0 ]
```

---

#### output

###### Options

| Property | Value | Description |
| ------ | ------ | ------ |
| data_source | - dir <br> - mongo | Set the source of the input, from a file in folder <br> or a MongoDb doc |
|domain| - stage <br> - master <br> - processed <br> - reports <br>  | Set the sub-area of the source. <br> Mongo and the folder system has the same structure. <br> That means you can combine each domain with the property data-source. <br> See the Router.py module. |
| collection | string | name of the MongoDb collection or <br> the folder in repository "domain" |
| add_date | bool | add current date in suffix output-file-name |

<br>

###### Access example
See the Reloader.py module, to access the metascript elements.

---

#### cmd
An example of the command string to call the script with arguments

```json
 "cmd":"001-myScriptName/python main.py search=#NBA counter=jordan-01",

```