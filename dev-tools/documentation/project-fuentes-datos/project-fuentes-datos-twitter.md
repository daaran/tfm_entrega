# Twitter

## Api Documentation
https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html

#### Api reference index
https://developer.twitter.com/en/docs/api-reference-index.html

#### Search
>API
https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html

>Standards
https://developer.twitter.com/en/docs/tweets/search/guides/standard-operators


## Links
>Interest
https://business.twitter.com/es/targeting/interest.html

>More
...


#### Stream.filter by location
It needs 4 coordinates.
Here's the first google result of a site that let's you draw bounding boxes. Select CSV format on the bottom-left of the page.
http://boundingbox.klokantech.com/
 You **cannot filter by both** location AND keyword.  But filter text in Listener.