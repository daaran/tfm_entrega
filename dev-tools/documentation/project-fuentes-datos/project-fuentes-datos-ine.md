# INE


#### Obtener identificadores de Tempus3 utilizando INEbase

Navegando a través de INEbase se pueden obtener los IDs que identifican de manera única a los elementos de la base de datos Tempus3:

http://www.ine.es/dyngs/INEbase/listaoperaciones.htm


#### Ejemplo:

1. http://www.ine.es/dyngs/INEbase/listaoperaciones.htm

2.  Cifras de población y Censos demográficos > **Cifras de población**
  http://www.ine.es/dyngs/INEbase/es/operacion.htm?c=Estadistica_C&cid=1254736176951&
  menu=ultiDatos&idp=1254735572981

3. Resultados > Series detalladas desde 2002 > Resultados por Provincias

4. **3.1 Población residente por fecha, sexo y edad**
    ````
    http://www.ine.es/jaxiT3/Tabla.htm?t=9687&L=0
    ````

5. Reemplaza la id  del parametro t= , en el enlace de descarga
    ````
    http://www.ine.es/jaxiT3/files/t/es/csv_c/9687.csv?nocab=1
    ````