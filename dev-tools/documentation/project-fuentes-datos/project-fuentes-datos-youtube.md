# Youtube APIs

https://developers.google.com/youtube/v3/

---
## API Development

https://developers.google.com/youtube/v3/getting-started?hl=es-419

##### Creacion de proyecto en la Google Apis
https://console.developers.google.com/projectcreate?previousPage=%2Fcloud-resource-manager&defaultProjectName&project&folder&organizationId=0

##### Panel
https://console.developers.google.com/apis/api/youtube.googleapis.com/overview?project=uoctfm-202716&hl=es-419&duration=PT1H
````      
Clave de API:  AIzaSyAO9_qassQJkc1Hwozt4d55OaNq4wFpfSE
Services: https://developers.google.com/apis-explorer/?hl=es_419#p/youtube/v3/
````      


##### API Library Python 
https://developers.google.com/youtube/v3/quickstart/python
https://developers.google.com/api-client-library/python/
https://developers.google.com/resources/api-libraries/documentation/youtube/v3/python/latest/
````
$ pip install --upgrade google-api-python-client
````  

##### Registrar aplicacion:
  https://developers.google.com/youtube/registering_an_application?hl=es-419

---
## Referencia

> Search 
https://developers.google.com/youtube/v3/docs/search/list#examples


---
## FAQs
https://stackoverflow.com/questions/tagged/google-apis-explorer

---
## Listado de APIs de Google:
https://console.developers.google.com/apis/library?project=uoctfm-202716&hl=es-419&q=Yout

