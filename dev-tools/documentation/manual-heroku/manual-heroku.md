# Heroku - Aplicacion Servidor

## Node.js
Para el alojamiento de la aplicacion servidor se utilizará..
https://devcenter.heroku.com


## Set up
https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up
Mediante la CLI de Heroku: <br>
### Creación de la aplicacion Node.js
En terminal como administrador:

1. Login
````
heroku login
Enter your Heroku credentials.
Email: user@example.com
Password:
````
2. Clona el repositorio de ejemplo de la Heroku en tu directory local.
3. Accede al directorio en la terminal
4. Crea la aplicacion en Heroku

````
cd RUTA/DIR_APP
heroku create
````
5. Deploy App - Commit
````
git add .
git commit -m "Comment"
git push heroku master
heroku open cool
````
> https://shielded-plateau-42619.herokuapp.com/

# Authorization

Generate a long-term token **✗**
https://devcenter.heroku.com/articles/oauth#web-application-authorization
https://devcenter.heroku.com/articles/platform-api-reference#oauth-client
````
heroku authorizations:create
````
> Token: 0123456789-a0123-01a2-acb0-012345b0123a

#### or

Retrieving the API token **✓** 
https://devcenter.heroku.com/articles/authentication
````
heroku auth:token
````
> Token: 0123456789-a0123-01a2-acb0-012345b0123a

# Sleep
https://devcenter.heroku.com/articles/free-dyno-hours
https://devcenter.heroku.com/articles/dynos#dyno-sleeping

Every Heroku account is allocated a pool of hours which you can use with Free dynos. Free dynos are unique because they go to sleep after 30 minutes of inactivity. This helps you conserve your Free dyno hours and so you can use them for fun, learning and experimentation. Other dyno types do not sleep or have a pool of hours.
