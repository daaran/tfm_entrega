# MongoDb

>**DB Version**: 3.6.4
>**Compass Version**: 1.13.1
>**Cloud URL**: https://cloud.mongodb.com


#### Table of Contents
1. [Documentation](#documentation)
    1.1. [Links](#links)
2. [Run MongoDb](#run-mongodb)
3. [Notes](#notes)
    3.1 [Object Document Mapper](#odm)
4. [Examples](#examples)
    4.1 [Run MongoDb](#run-mongodb)



---
##Documentation

###Links

- http://api.mongodb.com/python/current/tutorial.html
- http://api.mongodb.com/python/current/tools.html


---

##Run MongoDb
> 1.- In terminal to the "bin" directory where MongoDb is installed


```` sh
#mongod --dbpath [FULL PATH of TARGET DIRECTORY]

cd C:\Install-pc\MongoDB\bin
mongod.exe --dbpath D:\PRO__\UOC-Master\SEMESTRE-4\TFM_dev\PRO\universe\MONGO

````
> 2.- Run desktop MongoDB Compass

---

##Notes

###ODM
Object Document Mapper

I'd recommend looking into some ORM modules that have already been built. There are some out there - not 100% sure which ones are available for MongoDB, but the one I use (I am biased, I wrote it) is called ORB, and can be found at http://docs.projexsoftware.com/api/orb

MongoEngine is ODM (ORM-like, but for document-oriented database) that allows to work with MongoDB on Python. It uses simple declarative API similar to Django ORM.
**Repository:** https://github.com/MongoEngine/mongoengine
**Documentation:** http://docs.mongoengine.org/

