
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
   
    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    box.append("div")
      .attr("class","widget-flow")
      .attr("id","widget-flow-" + widgetId)

    let headerP = d3.select( '#header-' + widgetId );
    let defaultText = globalData.metascript.label;

    var fPallete = d3.schemePaired ;
    //fPallete[10] = '#de1fba';
    var palete = fPallete;
      
    var m = [10, 10, 10, 10];
    var w = wi; //- m[1] - m[3];
    var h = 150; //- m[0] - m[2];

    var modH = h - 20;
    var modW = w ;

    var svgContainer = box
      .append("div")
      .attr("width", w )
      .attr("height", h)
      .attr("id","div_" + widgetId)
      .attr("class","div-canvas");
      
    d3.selectAll("#div_" + widgetId + " > *").remove();

    headerP.text( defaultText );

    //###############################################################

    function addLegend(itemKey, iPos ){

      var glabel = d3.select("#ctrl" +  widgetId)
        .append("div")
        .attr("class","legend-item")
        .style("background-color",'#ccc' )
        .on("click", function() {
          
        })
        .append("span")
        .attr("class","legend-item-text")
        .text( itemKey )

    }


    function drawWidget( itemData, itemKey, iPos ){
    
      var g = d3.select("#div_" + widgetId)
        .append("div")
        .attr("class","div-row")

      g.append("div")
        .attr("class",function(d, i){
          return "div-row-left"
        })
        .text(itemKey)
      
      var gRight = g.append("div")
        .attr("class",function(d, i){
          return "div-row-right"
        })

      //Loop socials
      var social = []
      Object.keys(itemData).forEach(function( socialItem, sIdx ) {
        social.push( itemData[socialItem] );
      });

  
      gRight.append("div")
        .attr("class",function(d, i){
          return "div-row-g"
        })
        .selectAll("div")
        .data( social )
        .enter()
        .append("div")
        .attr("class",function(d, i){
          return "div-row-cell" + " social-" + i
        })
        .style("width", function( d, i){
          return linearScale( d.data[0].rate) + "%";
        })
        .style("background-color", function(d, i){
          return palete[1+ (i*2)];
        })
        .text(function( d, i){
          return d.data[0].rate + "%";
        })
        .attr("title", function( d, i){
          return d.attr.name + " " + d.data[0].name + " " + d.data[0].rate + "%";
        });

      gRight.append("div")
        .attr("class",function(d, i){
          return "div-row-g"
        })
        .selectAll("div")
        .data( social )
        .enter()
        .append("div")
        .attr("class",function(d, i){
          return "div-row-cell" + " social-" + i
        })
        .style("width", function( d, i){
          return linearScale( d.data[1].rate ) + "%";
        })
        .style("background-color", function(d, i){
          return palete[(i*2)];
        })
        .text(function( d, i){
          return d.data[1].rate + "%";
        })
        .attr("title", function( d, i){
          return d.attr.name + " " + d.data[1].name + " " + d.data[1].rate + "%";
        });
      
    
    }

    //######################################################################

    var min,max;
    var mms = []
    Object.keys(globalData).forEach(function( itemKey, idx) {
      if( itemKey !== "_id" && itemKey !== "metascript" ){
        var itemData = globalData[itemKey]
        Object.keys( itemData ).forEach(function( socialItem, sIdx ) {
          mms.push( itemData[socialItem].data[0].rate );
          mms.push( itemData[socialItem].data[1].rate );
        });
      }  
    });
    var max = d3.max( mms );
    var min = d3.min( mms );
    var linearScale = d3.scaleLinear().domain([min,max]).range( [ 20, 30 ] ); 
    
    //addLegend( "Facebook", 1);

    var products = [];
    Object.keys(globalData).forEach(function( itemKey, idx) {
      if( itemKey !== "_id" && itemKey !== "metascript" ){
        products.push( itemKey );
        drawWidget( globalData[itemKey], itemKey, idx )
      }  
    });

  }//Init

};
