
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
    
    const dataProp = 2; //Cambia 0,1,2 Prints(media), hits (media), o ctr %

    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    let headerP = d3.select( '#header-' + widgetId );
    headerP.text( globalData.dimension + ": "+  globalData.metadata.layers.data[dataProp].name + " " +   globalData.metadata.layers.data[dataProp].description  )
   
    var m = [10, 10, 10, 10];
    var w = wi - m[1] - m[3];;
    var h = 350 - m[0] - m[2];

    var svgContainer = box
      .append("svg")
      .attr("width", w )
      .attr("height", h)
      .attr("id","svg" + widgetId);


    var fPallete;
    if( globalData.layers.length < 10 ){
      fPallete = d3.schemeCategory10;

    }else{
      fPallete = d3.schemePaired;
      fPallete[10] = '#de1fba';
    }
    var palete = fPallete;


    var months = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"];

    var modH = h - 20;
    var modW = w - 40;

    var rect = svgContainer
            .selectAll("rect")
            .data(months)
            .enter()
            .append("rect");

    rect.attr("x", function( d,i ){ return (modW/12 )*i + 1;} )
      .attr("y", 0)
      .attr("width", (modW/12 )-1)
      .attr("height", modH)
      .attr("fill","#808080")
      .attr("fill-opacity", 0.1)
      .on("mouseover", function() {
        d3.select(this).classed("hover", true);
        })
      .on("mouseout", function() {
           d3.select(this).classed("hover", false);
      })


    svgContainer
      .selectAll("text")
      .data(months)
      .enter()
      .append("text")
      .attr("x", function( d,i ){ return (modW/12 )*i + 1;} )
      .attr("y", (modH+10))
      .text( function (d) { return d; })
      .attr("font-family", "sans-serif")
      .attr("font-size", "8px")
      .attr("fill", "#808080");


    var labelPos =[]
    
    //############################################################################

    function renderLayerProp( nLayer, nProp ){
     
      var color = globalData.layers[nLayer].color;
      var cityName = globalData.layers[nLayer].name;

      var polarY = [];
      globalData.layers[nLayer].data.forEach(function( d ) {
        polarY.push( linearScale( d[nProp] ) );
      });

      var dates = globalData.y.data;
      var difX =  (modW-5) / dates.length ;
      var polarX = dates.map(function(x, i) {
        return  (i * difX)+5;
      });
    
      var glabel = d3.select("#ctrl" +  widgetId)
        .append("div")
        .attr("class","widget-button-wrapper")
        .style("background-color",palete[color] )
        .on("click", function() {
          var statusButton = d3.select( this ).classed("inactive") ? true : false;
          if( statusButton ){
            d3.select( this ).style("background-color",palete[color] )
            d3.select( this ).classed("inactive",false)
          }
          else{
            d3.select( this ).style("background-color", "#cecece" )
            d3.select( this ).classed("inactive",true)
          }

          var layerTarget = d3.select("#svg" + widgetId ).select("#layer" + nLayer )
          var status = layerTarget.classed("hidden") ? false : true
          layerTarget.classed("hidden", status);
        })
        .append("span")
        .attr("class","widget-button")
        .text( cityName )


      var g = svgContainer
        .append("g")
        .attr("id","layer"+nLayer)
        .selectAll("g")
        .data(polarY)
        .enter()
        .append("circle")
        .attr("cx",function( d, i){ return polarX[i];} )
        .attr("cy",function( d, i){ return d;} )
        .attr("r",2)
        .attr('fill',palete[color])
        //.attr('fill-opacity', 0.5)
        .on("mouseover", function(d) {
          d3.select("#tooltip")
            .style("left", function( d, i){ return polarX[i] })
            .style("top", function( d, i){ return d;} )
            .transition()
            .style("visibility", "visible");
          d3.select("#series")
            .text( function( d, i){
              return globalData.metadata.layers.data[ nProp ].name +  ": " + lis[i][nProp] ;
            });
        })
     };

    //######################################################################

    var mms = [];

    globalData.layers.forEach( function( item, idx ){
      item.data.forEach( function( ditem, didx ){
        mms.push( ditem[dataProp] );
      } );
    } );
    
    var max = d3.max( mms );
    var min = d3.min( mms );
    var stepsY = ( (max-min) < 12 )? 1 : 5;
    var linearScale = d3.scaleLinear().domain([min,max]).range([modH, 0 ]); //reverse
    
    // if( (max - min ) > modH ){
    //   stepsY = modH;
    // }
   

    globalData.layers.forEach( function( item, idx ){
      renderLayerProp(idx, dataProp);
    } );

  
    
    function dec( y ){
      return Math.round( y ) ;
    }

    var lineData = d3.range(min, max, stepsY ).map( dec );

    svgContainer.selectAll("f")
      .data(lineData)
      .enter()
      .append("text")
      .attr("x",  modW + 8 )
      .attr("y", function( d, i ){ 
         return linearScale(d); 
      })
      .text( function( d,i ){  return d + "%"; } )
      .attr("font-family", "sans-serif")
      .attr("font-size", "8px")
      .attr("fill", "#808080");

    svgContainer
      .selectAll("text")
      .data(months)
      .enter()
      .append("text")
      .attr("x", function( d,i ){ return (modW/12 )*i + 1;} )
      .attr("y", (modH+10))
      .text( function (d) { return d; })
      .attr("font-family", "sans-serif")
      .attr("font-size", "8px")
      .attr("fill", "#808080");

    // var axisScale = d3.scaleLinear().domain([0, 365]).range([0+5, w]);
    // var xAxis = d3.axisBottom().scale(axisScale);
    // var xAxisGroup = svgContainer.append("g").call(xAxis);

  }



};
