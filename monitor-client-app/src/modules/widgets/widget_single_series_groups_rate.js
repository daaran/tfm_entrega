
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
   
    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    box.append("div")
      .attr("class","widget-flow")
      .attr("id","widget-flow-" + widgetId)

    let headerP = d3.select( '#header-' + widgetId );
    let defaultText = globalData.metascript.label;

    var palete = [
      "#62007F","#9D00CC","#D136FF",
      "#7F7200","#CCB600","#FFE400",
     // "#7F2B00","#CC4400","#FF5500",
      "#7F0038","#CC005A","#FF0070",
      "#387F00","#59CC00","#6FFF00",
      "#CC7000","#FF8C00","#FFA536",
      "#385A7F","#5991CC","#6FB5FF",
      "#5D2C7F","#9447CC","#B958FF",
      "#00427F","#006ACC","#0085FF",
      "#006C7F","#00ACCC","#00D7FF",
      "#7F1500","#CC2200","#FF2B00",
      "#367F5E","#56CC96","#6CFFBB"
    ];
      
    var m = [10, 10, 10, 10];
    var w = wi; //- m[1] - m[3];
    var h = 150; //- m[0] - m[2];

    var modH = h - 20;
    var modW = w ;

    var svgContainer = box
      .append("div")
      .attr("width", w )
      .attr("height", h)
      .attr("id","div_" + widgetId)
      .attr("class","div-canvas");
      
    var months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    headerP.text( defaultText );

    //###############################################################

    function addLegend( difMax ){

      d3.select("#ctrl" +  widgetId)
          .append('text')
          .attr("class","input-label")
          .text('Filtro diferencia respecto al maximo:' )

        d3.select("#ctrl" +  widgetId)
          .append('input')
          .attr('type','number')
          .attr('name','textInput')
          .attr('id','textInput_' +  widgetId)
          .attr("size","2")
          .attr("step", "0.1")
          .attr("min", "0.1")
          .attr("max", "0.8")
          .attr('value', difMax )

        d3.select("#ctrl" +  widgetId)
          .append('input')
            .attr('id','button_' + widgetId )
            .attr("type", "button")
            .attr("name", 'button_' + widgetId)
            .attr("value", "Redraw")
            .attr('class','button')
            .on("click", function( ){
              var iVal = d3.select('#textInput_' +  widgetId).property("value");
              drawAll( iVal );
            }); 
        

    }


    function drawWidget( productData, productKey, iPos, pDifMax ){
      
      var g = d3.select("#div_" + widgetId)
        .append("div")
        .attr("class","div-row")
        .style("background-color", function(d, i){
              return palete[0+ (iPos*3)];
        })


      g.append("div")
        .attr("class",function(d, i){
          return "div-row-left"
        })
        .text(productKey)
      
      var rowRight = g.append("div")
        .attr("class",function(d, i){
          return "div-row-right"
        })
        .style("background-color", function(d, i){
          return palete[1+ (iPos*3)];
        })

      //Loop socials
      var social = []
      Object.keys(productData).forEach(function( socialItem, sIdx ) {
        social.push( productData[socialItem] );
      });

  
      
      var socialRow = rowRight.selectAll("div")
        .data( social )
        .enter()
        .append("div")
        .attr("class",function(d, i){
          return "div-row-g"
        });
      
      socialRow.append("div")
        .attr("class",function(d, i){
          return "div-row-left"
        })
        .text(function( d, i){
          return Object.keys(productData)[i];//d.data[0].rate + "%";
        })

      var socialRigth =  socialRow.append("div")
        .attr("class",function(d, i){
          return "div-row-right"
        })
        .style("background-color", function(d, i){
          return palete[2+ (iPos*3)];
        })
        .each(function(d){
          d3.select(this)
            .selectAll('span')
            .data(d)
            .enter()
            .append('span')
            .attr("class", function( f, i){
              if(  f.data[0].data[0].rate < mean ){
                return "span-reg ignore";
              }
              else{
                if( f.data[0].data[0].rate > ( max - pDifMax) ){
                  return "span-reg top";
                }
                else{
                  return "span-reg";
                }
              }
            })
            .style("width", function( f, i){
              return Math.round( linearScale(f.data[0].data[0].rate)) + "%";
            })
            .text( function( f, i ){
              return  f.data[0].data[0].rate + "% ";
            })
            .attr( "title", function( f, i ){
              var info =  "Mes " + months[( f.attr["Month"] - 1) ]; 
              info += ", " + f.data[0].data[0].name + ": " + f.data[0].data[0].rate + "% "; 
              info += ", " + f.data[0].data[1].name + ": " + f.data[0].data[1].rate + "% ";

              return info;
            })
        })
 
    }

    //######################################################################

    //console.log( globalData )
    var min,max;
    var mms = []
    Object.keys(globalData).forEach(function( itemKey, idx) {
      if( itemKey !== "_id" && itemKey !== "metascript" ){
        var itemData = globalData[itemKey]
        Object.keys( itemData ).forEach(function( socialItem, sIdx ) {
          itemData[socialItem].forEach( function( mItem, mIdx ){
    
            mms.push( parseFloat( mItem.data[0].data[0].rate )  );
            //mms.push( parseFloat(mItem.data[0].data[1].rate ) );
            //mms.push( mItem.data[0].attr.current );
            //mms.push( mItem.data[0].attr.total );
          })
        });
      }  
    });
    var max = d3.max( mms );
    var min = d3.min( mms );
    var mean = d3.mean(mms);

    var difMax = 0.4;

    console.log( min, max, mean, (max- difMax) )
    var dif = min - (max - 8.33);
    var linearScale = d3.scaleLinear().domain([min,max]).range( [ dif, 8.33 ] ); 
    
    addLegend( difMax );
    var products = [];

    function drawAll( difMax ){
      d3.selectAll("#div_" + widgetId + " > *").remove();

      Object.keys(globalData).forEach(function( productKey, idx) {
        if( productKey !== "_id" && productKey !== "metascript" ){
          products.push( productKey );
          drawWidget( globalData[productKey], productKey, idx , difMax )
        }  
      });
    
    };

    drawAll( difMax );
  
  }//Init

};
