
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
   
    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    box.append("div")
      .attr("class","widget-flow")
      .attr("id","widget-flow-" + widgetId)

    let headerP = d3.select( '#header-' + widgetId );
    let defaultText = globalData.metascript.label;
      
    var w = 200;
    var h = w;
    var donutWidth = 50;  
    var radius = Math.min(w, h) / 2;
    var cs = ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]; 

   
    var socialColors = {
       youtube: ["#7F0000","#FF4C4C","#FF0000","#7F2626","#CC0000"],
       facebook : ["#00357F","#4C96FF","#0069FF","#264B7F","#0054CC"],
       twitter : ["#006F7F","#4CFFFC","#00FFFA","#267F7E","#00CCC8"],
       instagram : ["#63007F","#D74CFF","#C500FF","#6B267F","#9E00CC"]
    }

    var arc = d3.arc()
      .innerRadius(radius - donutWidth)
      .outerRadius(radius);

    var pie = d3.pie()
      .value(function(d) { return d.count; })
      .sort(null);

    headerP.text( defaultText );

    var listOptions = []; 
    
    var svgContainer = box
      .append("svg")
      .attr("width", wi )
      .attr("height", h + 20)
      .attr("id","svg" + widgetId)
      .append("g")
      .attr('transform', 'translate(' + (wi/2) + ',' + (h / 2) + ')');

    function addLegend( optionsGroup ){

      // d3.select("#ctrl" +  widgetId)
      //     .append('text')
      //     .attr("class","input-label")
      //     .text('Lorme ipsum' );

      var select = d3.select( '#ctrl' + widgetId )
        .append('select')
          .attr('id','select_' + widgetId )
          .attr('class','select')
          .on('change',onchangeRegion)

      var options = select
        .selectAll('option')
        .data(optionsGroup).enter()
        .append('option')
        .text(function (d, i) { 
          return d; 
        })
        .attr("value",function (d) { return d; });

      function onchangeRegion() {
        var selectValue = d3.select('#select_' + widgetId ).property('value');
        loadDataset( selectValue );
      };
    }

  
    function drawWidget( dataset, pKeyName ){

      var g = svgContainer.selectAll("g")
      .data(pie(dataset))
      .enter().append("g");  

      var path = g.append('path')
      .attr('d', arc)
      .attr('fill', function(d, i) {
        console.log( pKeyName.toLowerCase() );
        return socialColors[ pKeyName.toLowerCase() ][i];
        //return cs[i];
      });

      g.append("text")
        .attr("transform", function(d) {
          var _d = arc.centroid(d);
          _d[0] *= 1.5;	//multiply by a constant factor
          _d[1] *= 1.5;	//multiply by a constant factor
          return "translate(" + _d + ")";
        })
        .attr("dy", ".50em")
        .style("text-anchor", "middle")
        .attr("class","donut-label")
        .text(function(d) {
          return d.data.label + ": " + d.data.count + '%';
        });
      
      
    }

    var linearScale;
    var min, max;

    function loadDataset( pKeyName ){

      var dataset = [];
      var mms = [];
      d3.selectAll("#svg" + widgetId + " > g > *").remove();

      var socialBlock = globalData[ pKeyName ];
      Object.keys( socialBlock ).forEach(function( groupKey, idx ) {

        var vCount = parseFloat( socialBlock[ groupKey ].attr.ctr ); //Prints
        var item = {
          label: groupKey,
          count: vCount,
          prints: parseFloat( socialBlock[ groupKey ].data[ 0 ].rate ),
          hits: parseFloat( socialBlock[ groupKey ].data[ 1 ].rate )
        }
        mms.push( vCount )
        dataset.push( item );
          
      });

      //exagera diferencias
      max = d3.max( mms );
      min = d3.min( mms );
      
      linearScale = d3.scaleLinear().domain([min,max]).range( [ (min-2), (max+2) ] ); 

      var newDataset = [];

      dataset.forEach( function( item, id ){

        var item = {
          label: item.label,
          hits: item.hits,
          prints: item.prints,
          count: Math.round( linearScale( item.count ) * 100 ) / 100
        }
        newDataset.push( item );
        
      })
    
      drawWidget( newDataset, pKeyName );
    };

    //##################################
    
 
    Object.keys(globalData).forEach(function( groupKey, idx ) {
      if( groupKey !== "_id" && groupKey !== "metascript" ){
        listOptions.push( groupKey );
      }  
    });

    loadDataset( listOptions[0] );
    addLegend( listOptions );
    

  }//Init

};
