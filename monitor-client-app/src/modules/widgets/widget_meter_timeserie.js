
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
   
    //NAme: widget_simple

    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    box.append("div")
      .attr("class","widget-flow")
      .attr("id","widget-flow-" + widgetId)

    let headerP = d3.select( '#header-' + widgetId );
    let defaultText = globalData.metascript.label;
      
    var m = [10, 10, 10, 10];
    var w = wi; //- m[1] - m[3];
    var h = 150; //- m[0] - m[2];

    var svgContainer = box
      .append("svg")
      .attr("width", w )
      .attr("height", h)
      .attr("id","svg" + widgetId);

    var fPallete = d3.schemePaired;
    fPallete[10] = '#de1fba';
    var palete = fPallete;

    var platformSelectOptions = [];

    function drawWidget( cityName, pSocialId, pMonth ){

      d3.selectAll("#svg" + widgetId + " > *").remove();
      
      headerP.text( cityName + ", " + platformSelectOptions[pSocialId].label + ": "  +defaultText )

      let SOCIAL = pSocialId;

      var modH = h - 20;
      var modW = w ;

      function heightConversion( h ){
        return (h*modH) / 14000;
      }

      var index = Object.keys(globalData).indexOf(cityName);

      function filterMonth( objItem ){
        var sMonth =  parseInt( objItem.unique.split("-")[1] );
        if( pMonth === "-" || pMonth === undefined ){
          return true; 
        }else{
          return (sMonth === pMonth);
        }
        
      }

      var g = svgContainer
        .selectAll("g")
        .data( globalData[ cityName ].data )
        .enter()
        .append("g")
        //.filter( function(d) { return filterMonth(d); } )

      var daysFiltered = g._groups[0].length;

      var rect =g.append("rect")
        .attr("x", function( d,i ){ 
          return (modW/daysFiltered )* i + 1; //or 365
        } )
        .attr("y", function( d,i ){
          var o = d.blocks[SOCIAL].top;
          return modH - heightConversion( o )
        })
        .attr("width", function( d,i){
          return 2;
        } )
        .attr("height", function( d,i){
          var o = d.blocks[SOCIAL].top;
          return heightConversion( o );
        })
        .attr("fill","#aaa")
        .attr("fill-opacity", 0.5);

        rect.on("mouseover", function(d,u) {
          d3.select(this).classed("hover", true);
          d3.select( "#widget-flow-" + widgetId )
            .style("display", "block")
            .style("left", 0)
            .style("top", modH +"px")
            .text( function( f, i){
              return d.unique + " Prints: " + d.blocks[SOCIAL].top + ", Hits:" + d.blocks[SOCIAL].value;
            } )
        })
        .on("mouseout", function() {
          d3.select(this).classed("hover", false);
          d3.select( "#widget-flow-" + widgetId )
          .style("display", "none")
        });


      g.append("rect")
        .attr("x", function( d,i ){ 
          return (modW/daysFiltered )* i + 1; //or 365
        } )
        .attr("y", function( d,i){
          var o = d.blocks[SOCIAL].value;
          return modH - heightConversion( o )
        })
        .attr("width", function( d,i){
          return 2;
        } )
        .attr("height", function( d,i){
          var o = d.blocks[SOCIAL].value;
          return heightConversion( o );
        })
        .attr("fill", palete[index])
        .attr("fill-opacity", 0.5);

    }

    //------------------------------------------------------ SELECT 1
    var regionSelect = [];

    Object.keys(globalData).forEach(function(key) {

      if( globalData[key].attr ){
        regionSelect.push( key )
      }  
    });

    var select = d3.select( '#ctrl' + widgetId )
      .append('select')
        .attr('id','selectRegion' + widgetId )
        .attr('class','select')
        .on('change',onchangeRegion)
    
    var options = select
      .selectAll('option')
      .data(regionSelect).enter()
      .append('option')
        .text(function (d) { return d; })
        .attr("value",function (d) { return d; });
    
    function onchangeRegion() {
      let selectValue = d3.select('#selectRegion' + widgetId ).property('value')
      let socialIdx = d3.select('#selectPlatform' + widgetId ).property('value')
      drawWidget( selectValue, socialIdx );
    };
    //------------------------------------------------------

    // var monthsSelect = [];
    // var keyCursor = 0;
    // let firstKey = Object.keys(globalData)[ keyCursor ];
    // var listDays = globalData[firstKey].data[0];

    //@todo: Month not implemented

    //------------------------------------------------------ SELECT 2
    

    var keyCursor = 0;
    function selectPlatforms( ){
      let firstKey = Object.keys(globalData)[ keyCursor ];
      if( globalData[firstKey].attr ){
        
        let listPlatforms = globalData[firstKey].data[0].blocks;
        listPlatforms.forEach(function( item, id ) {
          platformSelectOptions.push( { "label": item.name, "idx": id } )
        });

        var select = d3.select( '#ctrl' + widgetId )
          .append('select')
            .attr('id','selectPlatform' + widgetId )
            .attr('class','select')
            .on('change',onchangePlatform)

        var options = select
          .selectAll('option')
          .data(platformSelectOptions).enter()
          .append('option')
            .text(function (d) { 
              return d.label; 
            })
            .attr("value",function (d) { return d.idx; });

        function onchangePlatform() {
          let selectValue = d3.select('#selectRegion' + widgetId ).property('value')
          let socialIdx = d3.select('#selectPlatform' + widgetId ).property('value')
          drawWidget( selectValue, socialIdx );
        };

      }
      else{
        keyCursor++;
        selectPlatforms();
      }
    }

    
  
    //------------------------------------------------------

    selectPlatforms();
    drawWidget( regionSelect[0], 0);

   

  }//Init

};
