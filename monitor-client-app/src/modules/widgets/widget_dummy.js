
'use strict';
import * as d3 from 'd3';

export default {

  init: ( widgetId, globalData ) => {
   
    let box = d3.select( '#d' + widgetId );
    let wi = box.node().getBoundingClientRect().width;

    box.append("div")
      .attr("class","widget-flow")
      .attr("id","widget-flow-" + widgetId)

    let headerP = d3.select( '#header-' + widgetId );
    let defaultText = globalData.metascript.label;
      
    var m = [10, 10, 10, 10];
    var w = wi;
    var h = 150;

    var svgContainer = box
      .append("svg")
      .attr("width", w )
      .attr("height", h)
      .attr("id","svg" + widgetId);

    //headerP.text( defaultText );
    
    var mainGroup = [];

    //#######################################################################

    function addLegend( ){

      // d3.select("#ctrl" +  widgetId)
      //     .append('text')
      //     .attr("class","input-label")
      //     .text('Lorme ipsum' );

      var select = d3.select( '#ctrl' + widgetId )
        .append('select')
          .attr('id','select_' + widgetId )
          .attr('class','select')
          .on('change',onchangeRegion)
      
      var options = select
        .selectAll('option')
        .data(mainGroup).enter()
        .append('option')
        .text(function (d, i) { 
          return d; 
        })
        .attr("value",function (d) { return d; });
      
      function onchangeRegion() {
        //let selectValue = d3.select('#selectRegion' + widgetId ).property('value')
        //let socialIdx = d3.select('#selectPlatform' + widgetId ).property('value')
        drawAll( );
      };
    }

 

    function drawWidget( groupData, groupKey, iPos ){
     

    }


    function drawAll( ){

      d3.selectAll("#svg" + widgetId + " > *").remove();

      Object.keys(globalData).forEach(function( groupKey, idx) {
        if( groupKey !== "_id" && groupKey !== "metascript" ){
          mainGroup.push( groupKey );
          drawWidget( globalData[groupKey], groupKey, idx )
        }  
      });
     
    };

    //##################################


    drawAll( );
    addLegend( )
    

  }//Init

};
