// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Vuex from 'vuex'

Vue.use(Vuex)

Vue.config.productionTip = false

const store = new Vuex.Store({
  strict: true,
  state: {
    selectedPanel:0,
    panelsData:[],
    isMenuOpen:false,
    //apiUrl:"http://localhost:5000",
    apiUrl:"https://shielded-plateau-42619.herokuapp.com",
    longTerm:"9d0cbc1c-8e0d-4e9e-8be1-c30b01c7bd98",
    apiToken: 'c4cd94da15ea0544802c2cfd5ec4ead324327430'
  },
  mutations: {
    setMenuOpen( state ){
      state.isMenuOpen = !state.isMenuOpen;      
    },
    setPanelsData( state, panels ){
      state.panelsData = panels;      
    },
    setSelectedPanel( iPanel ){
      state.selectedPanel = iPanel;      
    },
    getApiUrl(  ){
      return state.apiUrl;      
    },
    getLongTerm(  ){
      return state.longTerm;      
    },
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
